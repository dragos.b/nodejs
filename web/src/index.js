const express = require('express')
const path = require('path')
const app = express()


// Settings
app.set('port', 3000)                            // Reminds me to app.setPort (3000)
app.set('views', path.join(__dirname, 'views'))  // Configure views directory where ejs find the files to render
app.engine ('html', require('ejs').renderFile )  // Use the .html files like .ejs
app.set('view engine', 'ejs')                    // Use template engine EJS to render a file to html

// Static files
app.use(express.static(path.join(__dirname, 'public')))

// Routes
app.use(require('./routes/index'))

// Server Listen
app.listen(app.get('port'), () => {
    console.log('Server works on port', app.get('port'))
})

