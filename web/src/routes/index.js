const express = require('express')
const router = express.Router();


// Routes

router.get('/', (req, res) => {
    // Send file to localhost:3000
    // Render index file and send a object
    res.render('index.html', { title: 'Node Website' })
})

router.get('/about', (req, res) => {
    res.render('about.html', { title: 'About' })
})


router.get('/contact', (req, res) => {
    res.render('contact.html', { title: 'Contact' })
})


module.exports = router