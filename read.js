var http = require ( 'http' )
var fs   = require ( 'fs' )

const port = 8080

http.createServer ( function ( req, res ) {

    fs.readFile ( 'demo.html', function ( err, file ) {
        res.write   ( file )
        return res.end()
    })
}).listen ( port )
