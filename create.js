const http = require ( 'http' )
const fs   = require ( 'fs' )

const port  = 8080
const nfile = 'n_demo.html'


// Read and copy file to new file
fs.readFile ( 'demo.html', function (err, file) {
    fs.appendFile ( nfile, file, function(err) {
        if (err) throw err
    })
})

// Send to website the new file
http.createServer ( function ( req, res ) {
    fs.readFile ( nfile, function ( err, n_file ) {
        res.write ( n_file )
        res.end   ()
    })
}).listen ( port )
