// Like #include
const http     = require ( 'http' )

// Config
const hostname = 'localhost'
const port     = 8080


var title    = 'The World is a Vampire'

const server   = http.createServer ( ( req, res ) => {
    res.end ( `<h1> ${title} <h1/>` )
} );


// Turn on server
server.listen ( port, hostname, () => {
    console.log ( `Server is running at http://${hostname}:${port}/` )

})
